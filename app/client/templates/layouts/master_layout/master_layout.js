import 'admin-lte/dist/css/AdminLTE.min.css';
import 'admin-lte/dist/css/skins/skin-blue.min.css';
import 'admin-lte/dist/js/app';

Template.MasterLayout.helpers({
});

Template.MasterLayout.events({
});

Template.MasterLayout.onRendered(() => {
    $('body').addClass('skin-blue sidebar-mini');
});
