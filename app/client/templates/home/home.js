/*****************************************************************************/

/* Home: Event Handlers */
/*****************************************************************************/
Template.Home.events({
    'click #parse':function (e,t) {
        e.stopPropagation();
        e.preventDefault();
        Meteor.call('parse-descriptions',
            function(error, result) {
                if (result) {
                    console.log("Return true");
                   // Router.go('tablesList');
                }
            }
        );

    },
});

/*****************************************************************************/
/* Home: Helpers */
/*****************************************************************************/
Template.Home.helpers({
    descriptions: function(){
       return Descriptions.find();
    },

});

/*****************************************************************************/
/* Home: Lifecycle Hooks */
/*****************************************************************************/
Template.Home.onCreated(function () {
});

Template.Home.onRendered(function () {
});

Template.Home.onDestroyed(function () {
});
