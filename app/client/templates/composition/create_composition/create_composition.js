/*****************************************************************************/
/* CreateComposition: Event Handlers */
/*****************************************************************************/
var pathSelected = [];
Template.CreateComposition.events({
    'change select': function (e, t) {
        var currentTarget = e.currentTarget;
        var template = Template.instance();
        var descriptionPathsStrArrayComposition = [];
        var descriptionsArrayComposition = [];
        if (currentTarget.options[currentTarget.selectedIndex].value != "default") {
            var idSelect = currentTarget.dataset.id;
            var index = idSelect.substr(idSelect.lastIndexOf("-") + 1, idSelect.length);
            idSelect = idSelect.substr(0, idSelect.indexOf("-"));
            if (idSelect == "category") {
                var descriptionPaths;
                var pathsStr = [];
                var pathObj, id_path, pathStr;
                var PathidStrObj;
                var lastPath;
                var nameAPI = currentTarget.options[currentTarget.selectedIndex].value;
                descriptionPathsStrArrayComposition = template.descriptionPathArray.get();
                console.log(descriptionPathsStrArrayComposition);
                descriptionsArrayComposition = template.descriptionArray.get();
                //console.log(nameAPI);
                if(index < descriptionsArrayComposition.length - 1){
                    console.log("Entro cancella");
                    index++;
                    pathSelected.splice(index, pathSelected.length);
                    descriptionPathsStrArrayComposition.splice(index, descriptionPathsStrArrayComposition.length);
                    descriptionsArrayComposition.splice(index, descriptionsArrayComposition.length);
                    index--;
                }
                //descriptionPaths = Descriptions.findOne({nameAPI: nameAPI}).Path_Input;
                //  console.log(descriptionPaths);
                if(index > 0){
                    lastPath = pathSelected[index-1];
                    console.log("lastPath: " + lastPath);
                    Meteor.call('calculateCompositionPath',lastPath, nameAPI, function (error,result) {
                        if(result) {
                            console.log(result);
                            descriptionPathsStrArrayComposition[index] = result;
                            template.descriptionPathArray.set(descriptionPathsStrArrayComposition);
                            template.descriptionArray.set(descriptionsArrayComposition);
                        }else{
                            console.log(error);
                        }
                    });
                }else {
                    descriptionPaths = Descriptions.findOne({nameAPI: nameAPI}).Path_Input;
                    for (let indexPath in descriptionPaths) {
                        pathObj = descriptionPaths[indexPath];
                        //console.log("pathObj: " + pathObj);

                        id_path = pathObj.Id;
                        pathStr = pathObj.path;
                        PathidStrObj = {
                            "id_path": id_path,
                            "pathStr": pathStr
                        };

                        pathsStr.push(PathidStrObj);
                    }
                    descriptionPathsStrArrayComposition[index] = pathsStr;
                    Template.instance().descriptionPathArray.set(descriptionPathsStrArrayComposition);
                    Template.instance().descriptionArray.set(descriptionsArrayComposition);
                }
                console.log("Index: " + index);
                console.log("LunghezzaArrayComposition: " + descriptionsArrayComposition.length);
                /*if (index < descriptionsArrayComposition.length - 1) {
                    console.log("Index minore-Name");
                    descriptionPathsStrArrayComposition[index] = pathsStr;
                    index++;
                    descriptionPathsStrArrayComposition.splice(index, descriptionPathsStrArrayComposition.length);
                    descriptionsArrayComposition.splice(index, descriptionsArrayComposition.length);
                }*/


                //descriptionPathsStrArrayComposition[index] = pathsStr;

                console.log(descriptionsArrayComposition);
                console.log(descriptionPathsStrArrayComposition);

                /*Template.instance().descriptionPathArray.set(descriptionPathsStrArrayComposition);
                Template.instance().descriptionArray.set(descriptionsArrayComposition);*/

            }
            if (idSelect == "path") {
                var IdPath = currentTarget.options[currentTarget.selectedIndex].value;
                descriptionsArrayComposition = template.descriptionArray.get();
                pathSelected[index] = IdPath;
                descriptionPathsStrArrayComposition = template.descriptionPathArray.get();
                console.log("descriptionsArrayComposition: " + descriptionsArrayComposition);
                if (index < descriptionPathsStrArrayComposition.length - 1) {
                    console.log("Index minore-Path");
                    index++;
                    pathSelected.splice(index,pathSelected.length);
                    descriptionPathsStrArrayComposition.splice(index, descriptionPathsStrArrayComposition.length);
                    descriptionsArrayComposition.splice(index, descriptionsArrayComposition.length);
                    index--;
                }
                Meteor.call('calculateComposition', IdPath, function (error, result) {
                    console.log("Risultato: " + result);

                    // descriptionsArrayComposition = template.descriptionArray.get();
                    index++;
                    console.log("Index: " + index);
                    descriptionsArrayComposition[index] = result;
                    template.descriptionArray.set(descriptionsArrayComposition);
                    console.log("descriptionsArrayComposition: " + template.descriptionArray.get());
                    template.descriptionPathArray.set(descriptionPathsStrArrayComposition);
                    if (error) {
                        console.log("Error: " + error);
                    }
                });
                //console.log("descriptionsArrayComposition Dopo: " + Template.instance().descriptionArray.get());

            }
        }

    },


});

/*****************************************************************************/
/* CreateComposition: Helpers */
/*****************************************************************************/
Template.CreateComposition.helpers({

    descriptionsArray: function () {
        return Template.instance().descriptionArray.get();
    },

    descriptionPathArray: function (index) {
        return Template.instance().descriptionPathArray.get()[index];
    }


});

/*****************************************************************************/
/* CreateComposition: Lifecycle Hooks */
/*****************************************************************************/
Template.CreateComposition.onCreated(function () {
    this.descriptionArray = new ReactiveVar([]);
    this.descriptionPathArray = new ReactiveVar([]);
    this.descriptionIdPathArray = new ReactiveVar([]);
    this.viewDescriptionPathArray = new ReactiveVar([]);
});

Template.CreateComposition.onRendered(function () {
    var descriptionsName = [];
    var descriptionsArrayComposition = [];
    var viewDescriptionPathArray = [];
    Descriptions.find().forEach(function (description) {
        descriptionsName.push(description.nameAPI);
    });

    descriptionsArrayComposition.push(descriptionsName);
    // descriptionsName = [];
    // descriptionsName.push("Opac Unimib");
    // descriptionsArrayComposition.push(descriptionsName);
    viewDescriptionPathArray.push(false);
    Template.instance().descriptionArray.set(descriptionsArrayComposition);
    Template.instance().viewDescriptionPathArray.set(viewDescriptionPathArray);
});

Template.CreateComposition.onDestroyed(function () {
});
