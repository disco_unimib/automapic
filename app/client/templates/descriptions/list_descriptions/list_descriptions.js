/*****************************************************************************/
/* ListDescriptions: Event Handlers */
/*****************************************************************************/
Template.ListDescriptions.events({
    'click #parse': function (e, t) {
        e.stopPropagation();
        e.preventDefault();
        Meteor.call('parse-descriptions',
            function (error, result) {
                if (result) {
                    console.log("Return true");

                }
            }
        );

    },
    'click #composition': function (e, t) {
        e.stopPropagation();
        e.preventDefault();
        Meteor.call('composition', function (error, result) {
            if (result) {
                console.log("Return true");
            }

        })
    },
    'click #query': function (e,t) {
        e.stopPropagation();
        e.preventDefault();
        var risultato ;
        console.log("Entra in clickQuery");
        for(let i = 0 ; i< 20; i++) {
         console.log("i: " + i);
         Meteor.call('querySparql', 'Capital', function (error, result) {
         if (error) {
         console.log(error);
         }

         for (let res in result) {
         risultato = result[res].classAnnotation.value;
         console.log(risultato);
         }
         console.log(result);
         })
        }

    },
    'click #queryModificata': function(e,t){
        e.stopPropagation();
        e.preventDefault();
        for(let i = 0 ; i< 20; i++) {
            console.log("i: " + i);
            console.log("Entra in query modificata");
            Meteor.call('querymodificata', function (resutl, error) {
                if (error) {
                    console.log("Errore");
                } else {
                    console.log("Tutto ok");
                    console.log(result);
                }
            })
        }
        console.log("Fine query modificata");
    }
});

/*****************************************************************************/
/* ListDescriptions: Helpers */
/*****************************************************************************/
Template.ListDescriptions.helpers({
    descriptions: function () {
        return Descriptions.find();
    },
});

/*****************************************************************************/
/* ListDescriptions: Lifecycle Hooks */
/*****************************************************************************/
Template.ListDescriptions.onCreated(function () {
});

Template.ListDescriptions.onRendered(function () {
});

Template.ListDescriptions.onDestroyed(function () {
});
