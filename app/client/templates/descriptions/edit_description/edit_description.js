/*****************************************************************************/
/* EditDescription: Event Handlers */
/*****************************************************************************/
Template.EditDescription.events({
    'click #delete': function (e, t) {
        e.stopPropagation();
        e.preventDefault();
        var ident = this._id;
        console.log(ident);
        if (confirm('Really delete table\?')) {
            Meteor.call('description-delete', ident,
                function (error, result) {
                    if (result) {
                        Router.go('descriptions');
                    }
                }
            );
        }


    },
    'submit .updateDescription': function (e, t) {
        console.log("Cliccato update");
        e.stopPropagation();
        e.preventDefault();
        var ident = this._id;
        var nameAPI = e.target.nameAPI.value;
        var description = e.target.description.value;

        Meteor.call('description-update', ident, nameAPI, description,
            function (error, result) {
                if (result) {
                    Router.go('descriptions');
                }
            }
        );
    }
});

/*****************************************************************************/
/* EditDescription: Helpers */
/*****************************************************************************/
Template.EditDescription.helpers({
    nameAPI: function () {
        return Descriptions.findOne({_id: this._id}).nameAPI;
    },
    description: function () {
        return Descriptions.findOne({_id: this._id}).description;
    },

});

/*****************************************************************************/
/* EditDescription: Lifecycle Hooks */
/*****************************************************************************/
Template.EditDescription.onCreated(function () {
});

Template.EditDescription.onRendered(function () {
});

Template.EditDescription.onDestroyed(function () {
});
