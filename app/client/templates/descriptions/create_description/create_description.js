/*****************************************************************************/
/* CreateDescription: Event Handlers */
/*****************************************************************************/
Template.CreateDescription.events({
    'submit .createDescription': function (e, t) {
        console.log("createDescription");
        e.stopPropagation();
        e.preventDefault();
        var nameAPI = e.target.nameAPI.value;
        var file = t.find('#fileDescription').files[0];
        console.log("name file: " + file.name);
        console.log("NameAPI: " + nameAPI);
        var reader = new FileReader();
        reader.onload = function () {
            Meteor.call('file-upload', file.name, reader.result, nameAPI,
                function (error, result) {
                    if (result) {
                        console.log("File caricato");
                        Router.go('descriptions');

                    }
                }
            );
        };
        reader.onerror = function (error) {
            alert(error);
        };
        reader.readAsText(file);

    },
    'change input[type=file]'(event, t) {
        console.log(event);
        var file = t.find('#fileDescription').files[0];
        var reader = new FileReader();
        reader.onload = function () {
            t.find('#description').value = reader.result;

        };
        reader.onerror = function (error) {
            alert(error);
        };
        reader.readAsText(file);
    },
});

/*****************************************************************************/
/* CreateDescription: Helpers */
/*****************************************************************************/
Template.CreateDescription.helpers({});

/*****************************************************************************/
/* CreateDescription: Lifecycle Hooks */
/*****************************************************************************/
Template.CreateDescription.onCreated(function () {
});

Template.CreateDescription.onRendered(function () {
});

Template.CreateDescription.onDestroyed(function () {
});
