/*****************************************************************************/
/*  Server Methods */
/*****************************************************************************/
import parseInput from './parse/parse_descriptions';
import match from './composition/composition_description';
Meteor.methods({
    'parse-descriptions': function () {
        var count = 0;
        Descriptions.find().forEach(function (description) {
            count = 0;
            var descriptionJson = JSON.parse(description.description);
            var jsonArrayOfObject = parseInput(descriptionJson, count, description.nameAPI);
            Meteor.call('update-path_input_response', jsonArrayOfObject, description._id);
        });
        return true;
    },
    'description-delete': function (descriptionid) {
        Descriptions.remove({_id: descriptionid});
        return true;
    },
    'description-update': function (descriptionid, nameAPI, description) {

        Descriptions.update(
            {_id: descriptionid},
            {
                $set: {
                    "nameAPI": nameAPI,
                    "description": description
                },
            },
            {upsert: true}
        );
        return true;
    },
    'file-upload': function (fileInfo, fileData, nameAPI) {
        var responseObjs, descriptionJson, jsonArrayOfObject;
        var arrayInputMacth;
        Meteor._sleepForMs(3000);
        let original_id = new Mongo.ObjectID()._str;
        var description = {
            "_id": original_id,
            "nameAPI": nameAPI,
            "description": fileData
        };
        Descriptions.insert(description);
        descriptionJson = JSON.parse(description.description);
        jsonArrayOfObject = parseInput(descriptionJson, 0, description.nameAPI);

        if (Meteor.call('update-path_input_response', jsonArrayOfObject, description._id)) {
            description = null;
            var descriptions = Descriptions.find().fetch();
            description = Descriptions.find({'_id': original_id}).fetch();
            responseObjs = description[0].Response;
            arrayInputMacth = match(responseObjs, descriptions);
            Meteor.call('updateDescriptionMatch', original_id, arrayInputMacth);
        }
        return true;
    },
    'update-path_input_response': function (jsonArrayOfObject, original_id) {
        console.log("Entra in funzione");
        Descriptions.update(
            {_id: original_id},
            {
                $set: {"Path_Input": jsonArrayOfObject[0]}
            },
            {upsert: true}
        );
        Descriptions.update(
            {_id: original_id},
            {
                $set: {"Response": jsonArrayOfObject[1]}
            },
            {upsert: true}
        );
        return true;
    },
    'composition': function () {
        var responseObjs, arrayInputMacth;
        var descriptions = Descriptions.find().fetch();
        Descriptions.find().forEach(function (description) {
            responseObjs = description.Response;
            arrayInputMacth = match(responseObjs, descriptions);
            Meteor.call('updateDescriptionMatch', description._id, arrayInputMacth);

        });
    },
    'updateDescriptionMatch': function (descriptionId, arrayInputMacth) {
        Descriptions.update(
            {_id: descriptionId},
            {
                $set: {"Composition": arrayInputMacth}
            },
            {upsert: true}
        );
        return true;
    },
    'calculateComposition':function (pathId) {
        var infoCompositionNameAPI = [];
        var infoCompositionIdPathAPI = [];
        var infoCompositionPathAPI = [];
        var outputId = pathId + "_output";
        var nameAPI = pathId.substr(0,pathId.indexOf("_"));
        var descriptionCompositions;
        var compositionObj, id_input, id_output, nameComposition;
        var descriptionInputPath, inputObj, idPath, path;
        var infoNameAPIObj = {};

        descriptionCompositions = Descriptions.findOne({nameAPI: nameAPI}).Composition;
        for(let composition in descriptionCompositions){
            compositionObj = descriptionCompositions[composition];
            id_input = compositionObj.id_input;
            id_output = compositionObj.id_output;
             // console.log("id_input: " + id_input);
             // console.log("id_output: " + id_output);
             // console.log("outputId: " + outputId);
            if(id_output == outputId){
                nameComposition = id_input.substr(0,id_input.indexOf("_"));
                //console.log("nameComposition: " + nameComposition);
                infoCompositionNameAPI.push(nameComposition);
                nameAPI = id_input.substr(0,id_input.indexOf("_"));
                descriptionInputPath = Descriptions.findOne({nameAPI: nameAPI}).Path_Input;

                for(let input in descriptionInputPath){
                    inputObj = descriptionInputPath[input];
                    idPath = inputObj.Id;
                    path = inputObj.path;
                    if(idPath == id_input){

                        infoNameAPIObj = {
                            "idPath": idPath,
                            "path": path,
                        };
                        //infoCompositionIdPathAPI.push(idPath);
                        //infoCompositionPathAPI.push(path);
                        // console.log("idPath: " + idPath);
                        // console.log("path: " + path);
                    }
                }
            }
        }

        return infoCompositionNameAPI;
        
    },
    'calculateCompositionPath':function (lastPath, currentNameAPI) {
        var nameAPILastPath = lastPath.substr(0,lastPath.indexOf("_"));
        var descriptionCompositions, descriptionPath;
        var compositionObj, id_input, id_output;
        var descriptionInputPath, inputObj, idPath, path;
        var infoIdPathAPI;
        var infoCompositionPath = [];

        console.log("nameAPILastPath: " + nameAPILastPath);
        descriptionCompositions = Descriptions.findOne({nameAPI: nameAPILastPath}).Composition;
        for(let composition in descriptionCompositions) {
            compositionObj = descriptionCompositions[composition];
            id_input = compositionObj.id_input;
            id_output = compositionObj.id_output;
            if(id_input.substr(0,id_input.indexOf("_")) == currentNameAPI){
                descriptionInputPath = Descriptions.findOne({nameAPI: currentNameAPI}).Path_Input;
                for(let input in descriptionInputPath){
                    inputObj = descriptionInputPath[input];
                    idPath = inputObj.Id;
                    path = inputObj.path;
                    if(idPath == id_input){
                        infoIdPathAPI = {
                            "id_path": idPath,
                            "pathStr": path,
                        };
                        console.log("idPath: " + idPath);
                        console.log("path: " + path);
                        infoCompositionPath.push(infoIdPathAPI);
                    }
                }
            }
        }
        console.log(infoCompositionPath);
            return infoCompositionPath;
    },
    'querySparql':function (cityName) {
        var risultato;
        const {SparqlClient, SPARQL} = require('sparql-client-2');
        const client =
            new SparqlClient('http://dbpedia.org/sparql')
                .register({
                    db: 'http://dbpedia.org/resource/',
                    dbpedia: 'http://dbpedia.org/ontology/',
                    dbo :'http://dbpedia.org/ontology/',
                    rdfs:'http://www.w3.org/2000/01/rdf-schema#'
                });

        console.log("Entra in funzione fetchCityLeader");
      return client
            .query(SPARQL`
       SELECT distinct ?classAnnotation
       WHERE {
          ${{dbo: cityName}} rdfs:subClassOf* ?classAnnotation
       }`)
            .execute()
            // Get the item we want.
            .then(response => Promise.resolve(response.results.bindings))
            .catch(function (error) {
                console.log(error);
            });
       /* client
            .query(SPARQL`
       SELECT distinct ?classAnnotation
       WHERE {
          ${{dbo: cityName}} rdfs:subClassOf* ?classAnnotation
       }`)
            .execute()
            // Get the item we want.
            .then(function (results) {
                console.dir(results, {depth: null});
                console.log("RESULT: " + results.results.bindings);
                //console.log("RESULT.BINDING: " + result.bindings);
                risultato = results.bindings;
            })
            .catch(function (error) {
                console.log(error);
            });
            console.log("Risultato in query: " + risultato);
            return risultato;*/
        //Meteor._sleepForMs(3000);

        //return risultato;

    },
    'queryClassSemantic':function(classAnnotationProperty){
        const {SparqlClient, SPARQL} = require('sparql-client-2');
        console.log("classAnnotation in query: " + classAnnotationProperty);
        const client =
            new SparqlClient('http://dbpedia.org/sparql')
                .register({
                    db: 'http://dbpedia.org/resource/',
                    dbpedia: 'http://dbpedia.org/ontology/',
                    rdfs:'http://www.w3.org/2000/01/rdf-schema#'
                });
        return client
            .query(SPARQL`
       SELECT distinct ?classAnnotation
       WHERE {
          dbpedia:Library rdfs:subClassOf* ?classAnnotation
       }`)
            .execute()
            // Get the item we want.
            .then(response => Promise.resolve(response.results.bindings))
            .catch(function (error) {
                console.log(error);
            });
},
    'querymodificata':function () {
        console.log("Entro in funzione");
        /* var sparql = require('jsparql');

         var client = new sparql('http://dbpedia.org/sparql', 'http://dbpedia.org');

         client.query('select distinct ?Concept where {[] a ?Concept} LIMIT 10', function (err, results) {
         if (err) throw err;
         console.log(JSON.stringify(results));
         });
         }*/
        var sparql = require('jsparql');
        //var queryEntities = "select distinct ?Concept where {[] a ?Concept} LIMIT 10";
        //var prefix = 'PREFIX rdfs: <http//:www.w3.org/2000/01/rdf-schema#';
        var car = '<http://dbpedia.org/ontology/Library>';
        var queryEntities = 'PREFIX rdfs: <http://www.w3.org/2000/01/rdf-schema#> SELECT distinct ?classAnnotation WHERE { ' + car + 'rdfs:subClassOf* ?classAnnotation}';
       // for (let i = 0; i < 20; i++) {
           // console.log(i);
        sparqlClient = new sparql('http://dbpedia.org/sparql');
        resultEntities = sparqlClient.query({
            graph: 'http://dbpedia.org',
            query: queryEntities
        });
        try {
            resultEntities = JSON.parse(resultEntities).results.bindings;
            console.log("Risultato: " + JSON.stringify(resultEntities));
        } catch (err) {
            console.log(err);
            resultEntities = [];

        }
    //}
        console.log("Finita funzione modificata");
    }

});
