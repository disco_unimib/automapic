import { loadDescriptions } from './lib/load_description';
Meteor.startup(function () {
    loadDescriptions();
});
