/**
 * Created by lorenzo on 21/09/17.
 */


export default function parseInput(descriptionJson, count, nameAPI){
    var jsonArrayOfObjectInput = [];
    var jsonArrayObjectOutput = [];
    var jsonArrayObject = [];
    var jsonObject = {};
    var jsonArrayParam = [];
    var paths = descriptionJson.paths;
    var definitions = descriptionJson.definitions;
    var pathTxt;
    var descriptionTxt;
    var nameParamTxt;
    var inParamTxt;
    var defaultParamTxt;
    var requiredParamtTxt;
    var typeParamTxt;
    var enumParamTxt = [];
    var propertyAnnotationTxt;
    var classAnnotationTxt;


    for (let x in paths) {
        count = count + 1;
        jsonObject = {};
        //console.log('path: ' + x);
        pathTxt = x;
        let path = paths[x];
        for(let y in path){
            if(y == "get") {
                //console.log("y: " + y);
                if (path[y].description != null) {
                    //console.log('path.description: ' + path[y].description);
                    descriptionTxt = path[y].description
                }
                else {
                    //console.log('path.description: vuoto');
                    descriptionTxt = "Undefined"
                }
                if(path[y].parameters != null){
                    let param = path[y].parameters;
                    jsonArrayParam = [];
                    for (let j = 0; j < param.length; j++) {
                        nameParamTxt = checkNameParam(param[j].name);
                        inParamTxt = checkInParam(param[j].in);
                        defaultParamTxt = checkDefaultParam(param[j].default);
                        requiredParamtTxt = checkRequiredParam(param[j].required);
                        typeParamTxt = checkTypeParam(param[j].type);
                        enumParamTxt = checkEnumParam(param[j].enum);
                        propertyAnnotationTxt = checkpropertyParam(param[j].propertyAnnotation);
                        classAnnotationTxt = checkClassParam(param[j].classAnnotation);
                        jsonArrayParam.push(createObjectParam(nameParamTxt, inParamTxt, defaultParamTxt, requiredParamtTxt, typeParamTxt, propertyAnnotationTxt, classAnnotationTxt, enumParamTxt));
                    }
                }
                var response = path[y].responses;
                //console.log("Chiamo funzione parsingResponse");
                jsonArrayObjectOutput.push(parsingResponse(response, count, nameAPI, definitions));
            }
        }
        jsonArrayOfObjectInput.push(createObjectInputPath(jsonArrayParam, pathTxt, descriptionTxt, count, nameAPI));
    }
    //console.log("Array di Input: " + jsonArrayOfObject);
    var str = JSON.stringify(jsonArrayOfObjectInput, null, 2);
    //console.log("str: " + str);
    jsonArrayObject.push(jsonArrayOfObjectInput,jsonArrayObjectOutput);
    return jsonArrayObject;

}

function checkNameParam(name) {
    if(name != null){
        return name;
    }
    else{
        return "Undefined";
    }
}
function checkInParam(inPathQuery)
{
    if(inPathQuery != null){
        return inPathQuery;
    }
    else{
        return "Undefined";
    }
}
function checkDefaultParam(
    defaultTxt)
{
    if(defaultTxt != null){
        return defaultTxt;
    }
    else{
        return "Undefined";
    }
}
function checkRequiredParam(required) {
    if(required != null){
        return required;
    }
    else{
        return "Undefined";
    }
}
function checkTypeParam(type) {
    if(type != null){
        return type;
    }
    else{
        return "Undefined";
    }
}
function checkEnumParam(enumParam) {
    var myArray = [];
    if (enumParam != null) {

        for (let j = 0; j < enumParam.length; j++) {
            myArray.push(enumParam[j]);
        }
    }
    else{
        myArray.push("Undefined");
    }
    return myArray;
}
function checkClassParam(classAnnotation: any) {
    if(classAnnotation != null){
        return classAnnotation;
    }
    else{
        return "Undefined";
    }
}
function checkpropertyParam(propertyAnnotation: any) {
    if(propertyAnnotation != null){
        return propertyAnnotation;
    }
    else{
        return "Undefined";
    }
}
function createObjectParam(nameParamTxt: any, inParamTxt: any, defaultParamTxt: any, requiredParamtTxt: any, typeParamTxt: any, propertyAnnotationTxt: any, classAnnotationTxt: any, enumParamTxt: any) {
    var myObject = {};
    myObject = {
        "name" : nameParamTxt,
        "in" : inParamTxt,
        "type" : typeParamTxt,
        "required" : requiredParamtTxt,
        "default" : defaultParamTxt,
        "enum"  :  enumParamTxt,
        "classAnnotation" : classAnnotationTxt,
        "propertyAnnotation" : propertyAnnotationTxt
    };
    return myObject;

}
function createObjectInputPath(jsonArrayParam: any, pathTxt: any, descriptionTxt: any, count: any, nameAPI: any) {
    var myObject = {};
    var Id = nameAPI + "_" + count;
    myObject = {
        "Id" : Id,
        "path": pathTxt,
        "description" : descriptionTxt,
        "parameters" : jsonArrayParam
    };

    return myObject;
}



function parsingResponse(response, count, nameAPI, definitions){
    //console.log("Inizio parsing response: " + response);
    var arrayObjectProperties = [];
    var jsonObjectResponse = {};
    var str = "";
    var ref = [];
    var id_output = nameAPI + "_" + count + "_" + "output";
    var contex = [];
    var depth = 0;
    for(let resp in response){
        //console.log("resp: " + resp);
        if(resp == "200"){
            let schema = response[resp].schema;
            if(schema.type == null){
                //console.log("Schema Ref: " + schema.$ref);
                str = schema.$ref;
                ref = str.split("/");
                arrayObjectProperties = parseRef1(ref[2],definitions, arrayObjectProperties, contex);
            }else{
                if(schema.type == "object"){
                    //console.log("Entra in object");
                    arrayObjectProperties = parseProperties(schema.properties,definitions, arrayObjectProperties, contex);
                }
            }
        }
    }
    //console.log("arrayObjectProperties:" + arrayObjectProperties[0].nameProperties);

    jsonObjectResponse = {
        "id_output": id_output,
        "properties": arrayObjectProperties
    }
    return jsonObjectResponse;
}

function parseRef1(ref, definitions, arrayObjectProperties, contex){
    //console.log("Entro in parseRef1");
    var propertiesContext = [];
    var str = "";
    var refe = [];
    var jsonObjectProperties = {};
    //console.log("contex[] in Ref: " + contex);
    for(let d in definitions){
        if(ref == d){
            //contex.push(definitions[d]);
            //console.log("ref == d: " + d);
            propertiesContext.push(definitions[d]);
            let resp = definitions[d];
            //console.log("resp" + JSON.stringify(resp, null, 2));
            if(resp.type == "object"){
                //console.log("parseRef1 entra object");
                parseProperties(resp.properties,definitions, arrayObjectProperties, contex);
            }
            if(resp.type == "array"){
                //console.log("parseRef1 entra array");
                let item =  resp.items;
                //console.log("item" + JSON.stringify(item, null, 2));
                if(item.type == "object"){
                    //console.log("parseRef1 Entra in array poi object");
                    parseProperties(item.properties,definitions, arrayObjectProperties, contex);
                }
                if(item.type == null && item.$ref != null){
                    //console.log("parseRef1 Entra in array poi null");
                    str = item.$ref;
                    refe = str.split("/");
                    parseRef1(refe[2],definitions, arrayObjectProperties, contex);
                }
                if(item.type != "object" && item.type != "array" && item.type != null && item.$ref == null){
                    jsonObjectProperties = createJsonObjectProperties(ref, item.type, item.classAnnotation, item.propertyAnnotation, contex);
                    contex = [];
                    //console.log("contex = [] dopo: " + contex);
                    //console.log("jsonObjectProperties: " + JSON.stringify(jsonObjectProperties, null, 2));
                    arrayObjectProperties.push(jsonObjectProperties);
                }
            }
        }
    }
    return arrayObjectProperties;
}
function createJsonObjectProperties(p: string, type, classAnnotation: any, propertyAnnotation: any, contex) {
    var myObject = {};
    //console.log("contex = [] prima: " + contex);
    myObject = {
        "nameProperties" : p,
        "type": type,
        "contex": contex,
        "classAnnotation" : classAnnotation,
        "propertyAnnotation" : propertyAnnotation
    };

    return myObject;

}
function parseProperties(properties, definitions, arrayObjectProperties, contex){
    //console.log("Entra in parseProperties");
    var jsonObjectProperties = {};
    var str = "";
    var ref = [];
    for(let p in properties){

        contex.push(p);
        //console.log("p: " + p);
        //console.log("contex[] in Properties: " + contex);
        //console.log("properties[p].type: " + properties[p].type);
        if(properties[p].type == "object"){
            //console.log("parseProperties entra object");
            parseProperties(properties[p].properties, definitions, arrayObjectProperties, contex);
        }else{
            if(properties[p].type == "array"){
                //console.log("parseProperties entra array");
                let item =  properties[p].items;
                if(item.$ref != null){
                    str = item.$ref;
                    ref = str.split("/");
                    parseRef1(ref[2],definitions, arrayObjectProperties, contex
                    );
                }else{
                    //console.log("Nome: " + p);
                    //console.log("Type: " + properties[p].type);
                    //console.log("ClassAnnotation: " + properties[p].classAnnotation);
                    //console.log("PropertyAnnotation: " + properties[p].propertyAnnotation);
                    jsonObjectProperties = createJsonObjectProperties(p, properties[p].type, properties[p].classAnnotation, properties[p].propertyAnnotation, contex);
                    contex = [];
                    //console.log("contex = [] dopo: " + contex);
                    //console.log("jsonObjectProperties: " + JSON.stringify(jsonObjectProperties, null, 2));
                    arrayObjectProperties.push(jsonObjectProperties);
                }

            }
            if(properties[p].type != "object" && properties[p].type != "array" && properties[p].type != null){
                //console.log("Entro nell'IF lungo");
                //console.log("properties[p].type: " + properties[p].type);
                jsonObjectProperties = createJsonObjectProperties(p, properties[p].type, properties[p].classAnnotation, properties[p].propertyAnnotation, contex);
                contex = [];
                //console.log("contex = [] dopo: " + contex);
                //console.log("jsonObjectProperties: " + JSON.stringify(jsonObjectProperties, null, 2));
                arrayObjectProperties.push(jsonObjectProperties);
                //console.log("Nome: " + p);
                //console.log("Type: " + properties[p].type);
                //console.log("ClassAnnotation: " + properties[p].classAnnotation);
                //console.log("PropertyAnnotation: " + properties[p].propertyAnnotation);

            }
            if(properties[p].type == null){
                //console.log("parseProperties entra null");
                str = properties[p].$ref;
                ref = str.split("/");
                //console.log("ref: " + ref[2]);
                parseRef1(ref[2],definitions, arrayObjectProperties, contex);
            }
        }
    }

    //console.log("contex[] in Properties fine CICLO  : " + contex);
    return arrayObjectProperties;
}