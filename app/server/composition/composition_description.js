/**
 * Created by lorenzo on 03/10/17.
 */
const {SparqlClient, SPARQL} = require('sparql-client-2');

export default function match(responseObjs,descriptionArrayInput): void {
    console.log("Entro in match");
    var inputObjs;
    var inputObjectSave;
    var arrayInputMacth = [];
    var i = 0;
    console.log("responseObjs!!!:" + JSON.stringify(responseObjs));
        for(let responseObj in responseObjs){
            //console.log("Entro");
            for(let input in descriptionArrayInput){
                //console.log("Entro qua");
                inputObjs = descriptionArrayInput[input].Path_Input;
                for(let inputObj in inputObjs){
                    inputObjectSave = matchPropertiesParameter(responseObjs[responseObj],inputObjs[inputObj],i);
                    if(inputObjectSave != null){
                        arrayInputMacth.push(inputObjectSave);
                        console.log("inputObjectSave: " + JSON.stringify(inputObjectSave));
                    }else{
                        console.log("Non salvare niente!");
                    }
                }
            }
        }
        console.log("RESPONSE PROCESSATA, PASSA ALLA PROSSIMA!!!!!!");
     console.log("updateDescriptionMatch:" + JSON.stringify(arrayInputMacth));

    return arrayInputMacth;
}

function matchPropertiesParameter(output, input,i){
    var match = 0;
    var parameterObj;
    var propertyObj;
    var inputObject = {};
    var parameterObject;
    var nameParameters;
    var arrayNameProperties = [];
    var arrayParameters = [];
    var verificated = false;
    var id_output = output.id_output;
    console.log("output.id_output: " + output.id_output);
    var id_input = input.Id;
    console.log("input.Id: " + input.Id);
    var properties = output.properties;
    var parameters = input.parameters;
    for(let parameter in parameters){
        //console.log("Valoreeeeeee di i: " + i);
        i++;
        verificated = false;
        parameterObj = parameters[parameter];
        nameParameters = parameterObj.name;
        for(let property in properties){
            propertyObj = properties[property];
           /* if((id_output == 'Whattaspot_1_output' && id_input == 'Google Geocoding_1') || (id_output == 'Google Geocoding_1_output' && id_input == 'Marine Weather_1')){
                console.log("--------------------------------------------------------");
                console.log("propertyObj.classAnnotation: " + propertyObj.classAnnotation);
                console.log("parameterObj.classAnnotation: " + parameterObj.classAnnotation);
                console.log("propertyObj.propertyAnnotation: " + propertyObj.propertyAnnotation);
                console.log("parameterObj.propertyAnnotation: " + parameterObj.propertyAnnotation);
            }*/
            if((equalsString(propertyObj,parameterObj) || equalsClassSemantic(propertyObj,parameterObj)) && verificated == false){
                console.log("MATCH");
                console.log("output.id_output: " + output.id_output);
                console.log("input.Id: " + input.Id);
                console.log("propertyObj.classAnnotation: " + propertyObj.classAnnotation);
                console.log("parameterObj.classAnnotation: " + parameterObj.classAnnotation);
                console.log("propertyObj.propertyAnnotation: " + propertyObj.propertyAnnotation);
                console.log("parameterObj.propertyAnnotation: " + parameterObj.propertyAnnotation);
                arrayNameProperties.push(propertyObj.nameProperties);
                //verificated = true;
                match ++;
            }else{
                if((parameterObj.default != 'Undefined' || parameterObj.enum[0] != 'Undefined' || parameterObj.required == 'false') && verificated == false){
                    match++;
                    verificated = true;
                    console.log("Caso particolare: " + parameterObj.name);
                }
            }

            //match = 0;
        }
        console.log("Fine Controllo propertyObj,parameterObj");
        parameterObject = {
            "nameParameter": nameParameters,
            "properties": arrayNameProperties
        };
        arrayNameProperties = [];
        arrayParameters.push(parameterObject);
    }
    console.log("Numero finale di macth: " + match);
    if(match >= parameters.length){
        console.log("API con OUTPUT " + output.id_output + " può essere combinato con API con INPUT " + input.Id);
        inputObject = {
            "id_output" : id_output,
            "id_input" : id_input,
            "parameters" : arrayParameters
        };
        console.log("inputObject: " + JSON.stringify(inputObject));
    }else {
        console.log("inputObject: VUOTO");
        inputObject = null;
    }
    return inputObject;
}
function equalsString(propertyObj,parameterObj){
    if(propertyObj.classAnnotation != 'class' && parameterObj.classAnnotation != 'class' && propertyObj.propertyAnnotation != 'prop' && parameterObj.propertyAnnotation != 'prop') {
        if ((propertyObj.classAnnotation == parameterObj.classAnnotation && propertyObj.classAnnotation == 'rdfs:XMLLITERAL' && propertyObj.propertyAnnotation == parameterObj.propertyAnnotation) || propertyObj.propertyAnnotation == parameterObj.propertyAnnotation || (propertyObj.classAnnotation == parameterObj.classAnnotation && propertyObj.classAnnotation != 'rdfs:XMLLITERAL')) {
            return true;
        } else {
            return false;
        }
    }
}
function equalsClassSemantic(propertyObj,parameterObj) {

    var result_one;
    var trovato = false;
    if (propertyObj.classAnnotation != 'class' && parameterObj.classAnnotation != 'class' && propertyObj.classAnnotation.toLowerCase() != 'rdfs:xmlliteral' && parameterObj.classAnnotation != 'rdfs:xmlliteral') {
        console.log("Entro in IF");
        console.log("In equalSemantic: " + propertyObj.classAnnotation);
        /*Meteor.call('queryClassSemantic',propertyObj.classAnnotation,function (error,result) {
         if (error) {
         console.log(error);
         }
         console.log("Fine query");
         for(let res in result){
         //console.log(result[res].classAnnotation);
         console.log(result[res].classAnnotation.value);
         if(result[res].classAnnotation.value == parameterObj.classAnnotation){
         trovato = true;
         }
         //console.log(risultato);
         }
         if(trovato){
         return true;
         }else{
         return false;
         }
         })*/
        result_one = queryClassSemantic(propertyObj.classAnnotation);
        console.log("Fine query");
        console.log("Risultato: " + JSON.stringify(result_one));
        console.log("Risultato Senza JSON: " + result_one);
        var classAnnotationObjects = result_one;
        var classAnnotationStr = "";
        for(let classAnnotationObject in classAnnotationObjects){
            //console.log(JSON.stringify(classAnnotationObject));
            classAnnotationStr = classAnnotationObjects[classAnnotationObject].classAnnotation.value;
            console.log(classAnnotationStr);
            if(classAnnotationStr == parameterObj.classAnnotation){
                console.log("TROVATO SEMANTICOOOOOOOOOOOOOOOOOOOOOOO    ");
                trovato = true;
            }
        }
        //Meteor._sleepForMs(10000);
        /*initPromise().then(function(result) {
         console.log(result); // "initResolve"
         return "normalReturn";
         })
         .then(function(result) {
         console.log(result); // "normalReturn"
         });*/
        //Meteor._sleepForMs(8000);
        /*result_one.then(function (result) {
         console.log("Entro in funzione--------------------------------------");
         //console.log(result);
         for(let res in result){
         //console.log(result[res].classAnnotation);
         console.log(result[res].classAnnotation.value);
         if(result[res].classAnnotation.value == parameterObj.classAnnotation){
         trovato = true;
         }
         //console.log(risultato);
         }
         if(trovato){
         return true;
         }else{
         return false;

         }
         console.log("Finita funzione equalsClassSemantic");
         })
         }*/
        return trovato;

    }
}
    function queryClassSemantic(classAnnotationProperty) {
        console.log("classAnnotation in query: " + classAnnotationProperty);
        /*const client =
         new SparqlClient('http://dbpedia.org/sparql')
         .register({
         db: 'http://dbpedia.org/resource/',
         dbpedia: 'http://dbpedia.org/ontology/',
         rdfs:'http://www.w3.org/2000/01/rdf-schema#'
         });
         return client
         .query(SPARQL`
         SELECT distinct ?classAnnotation
         WHERE {
         ${{classAnnotationProperty}} rdfs:subClassOf* ?classAnnotation
         }`)
         .execute()
         // Get the item we want.
         .then(response => Promise.resolve(response.results.bindings))
         .catch(function (error) {
         console.log(error);
         });*/

        ////////////////////////////////
        var sparql = require('jsparql');
        //var car = '<http://dbpedia.org/ontology/Library>';
        var car = "<" + classAnnotationProperty + ">" + " ";
        var queryEntities = 'PREFIX rdfs: <http://www.w3.org/2000/01/rdf-schema#> SELECT distinct ?classAnnotation WHERE { ' + car + 'rdfs:subClassOf* ?classAnnotation}';
        console.log("QUERY: " + queryEntities);
        //var queryEntities = "PREFIX rdfs: <http://www.w3.org/2000/01/rdf-schema#> SELECT distinct ?classAnnotation WHERE { <http://dbpedia.org/ontology/Library> rdfs:subClassOf* ?classAnnotation}";
        // for (let i = 0; i < 20; i++) {
        // console.log(i);
        sparqlClient = new sparql('http://dbpedia.org/sparql');
        resultEntities = sparqlClient.query({
            graph: 'http://dbpedia.org',
            query: queryEntities
        });
        try {
            resultEntities = JSON.parse(resultEntities).results.bindings;
            console.log("Risultato in funzione queryClassSemantic: " + JSON.stringify(resultEntities));
        } catch (err) {
            console.log("TROVATO ERROREEEEEEE!!!");
            console.log(err);
            resultEntities = [];

        }
        console.log("FIne funzione queryClassSemantic");
        return resultEntities;

    }





