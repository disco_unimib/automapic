Router.configure({
  layoutTemplate: 'MasterLayout',
  loadingTemplate: 'Loading',
  notFoundTemplate: 'NotFound'
});

Router.route('/', {
  name: 'home',
  controller: 'HomeController',
  where: 'client'
});

Router.route('/descriptions', {
  name: 'descriptions',
  controller: 'DescriptionsController',
  where: 'client',
  action: 'list'
});

Router.route('/descriptions/create', {
    name: 'createDescription',
    controller: 'DescriptionsController',
    where: 'client',
    action: 'create'
});

Router.route('/descriptions/:_id', {
    name: 'editDescription',
    controller: 'DescriptionsController',
    where: 'client',
    action: 'edit'
});

Router.route('/compositions/create', {
    name: 'createComposition',
    controller: 'CompositionController',
    where: 'client',
    action: 'create'
});
