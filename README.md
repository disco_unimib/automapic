# AutomAPIc tool

AutomAPIc™ is a comprehensive tool to manage semantic descriptions and input/ouput composition of services.

## Getting Started

These instructions will get you a copy of the project up and running on your local machine for development and testing 
purposes. See deployment for notes on how to deploy the project on a live system.

### Prerequisites

1. Download the Node.js installer for your platform at [Nodejs.org](https://nodejs.org/en/download/)
2. Download the Meteor installer at [Meteor.com](https://www.meteor.com/install)
3. Install the iron command line tool globally so you can use it from any project directory

```
$ npm install -g iron-meteor
```

### Installing

Download the project dependencies from ./app directory, launching the following command:

```
$ meteor npm install
```

## Running

From the root directory of the project, launch the following command:

```
$ iron run
```

Open the web browser and go to <http://localhost:3000/> for testing the tool.

## Authors

Marco Cremaschi

## License

```
Copyright 2018 University of Milano - Bicocca

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at [apache.org/licenses/LICENSE-2.0](http://www.apache.org/licenses/LICENSE-2.0)

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
```